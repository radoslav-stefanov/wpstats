<?php
/**
* Plugin Name: wpstats by wordpressko
* Plugin URI: https://gitlab.com/wordpressko/wpstats
* Description: Export basic WordPress metrics for monitoring purposes.
* Author: wordpressko.bg
* Author URI: https://gitlab.com/wordpressko.bg
* Version: 0.8b
*/

function retrieve_metrics( $data ) {
  return "";
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'metrics', '/', array(
    'methods' => 'GET',
    'callback' => 'retrieve_metrics',
  ) );
} );

function get_wordpress_metrics(){
  $result="";
  $result.="# HELP wp_users_total Total number of users.\n";
  $result.="# TYPE wp_users_total counter\n";
  $total_users=count_users();
  $result.='wp_users_total '.$total_users['total_users']. "\n";

  $posts=wp_count_posts();
  $published_posts=$posts->publish;
  $draft_posts=$posts->draft;
  $pages=wp_count_posts('page');

  $comments=wp_count_comments();
  $total_comments = $comments->approved;
  $result.="# HELP wp_comments_total Total number of comments.\n";
  $result.="# TYPE wp_comments_total counter\n";
  $result.='wp_comments_total{status="approved"} '.$total_comments."\n";

  $result.='wp_posts_total{status="published"} '.$published_posts."\n";
  $result.="# HELP wp_posts_draft_total Total number of posts published.\n";
  $result.="# TYPE wp_posts_draft_total counter\n";
  $result.='wp_posts_total{status="draft"} '.$draft_posts."\n";

  $result.="# HELP wp_pages_total Total number of posts published.\n";
  $result.="# TYPE wp_pages_total counter\n";
  $result.='wp_pages_total{status="published"} '.$pages->publish."\n";
  $result.='wp_pages_total{status="draft"} '.$pages->draft."\n";
  return $result;
}

function multiformat_rest_pre_serve_request( $served, $result, $request, $server ) {
  if ( $request->get_route()=="/metrics" ) {
    header( 'Content-Type: text/plain; charset=' . get_option( 'blog_charset' ) );
    $metrics=get_wordpress_metrics();
    echo $metrics;
    $served = true;
  }
  return $served;
}

add_filter( 'rest_pre_serve_request', 'multiformat_rest_pre_serve_request', 10, 4 );
